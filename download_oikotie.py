"""
Run this script to download house sales data from oikotie.fi
To see usage, run: > python download_oikotie.py -h
"""

import sys, os
# import urllib, urllib2
import requests
import time, math
import argparse as parse
import json

MODE_DICT = {0: "sales", 1: "rentals"}

SALES_REQUEST_PREFIX = "http://asunnot.oikotie.fi/apartment-sell/search/card?" + \
	"asprice%5Bsuffix%5D=000&asprice%5Bmin%5D=&asprice%5Bmax%5D=" + \
	"&asprice%5Bunpriced%5D%5B%5D=1&assize%5Bmin%5D=&assize%5Bmax%5D=" + \
	"&assettings%5Bchanged%5D=0&assettings%5Bcollapsed%5D=1" + \
	"&asbuildyear%5Bmin%5D=&asbuildyear%5Bmax%5D=&assizelot%5Bmin%5D=" + \
	"&assizelot%5Bmax%5D=&asnewdevelopment%5Bnew_development%5D=1" + \
	"&aspublished%5Bpublished%5D=1&offset="

RENTALS_REQUEST_PREFIX =  'http://asunnot.oikotie.fi/apartment-rent/search/card?\
	arpricerent%5Bmin%5D=&arpricerent%5Bmax%5D=&arsize%5Bmin%5D=\
	&arsize%5Bmax%5D=&arsettings%5Bchanged%5D=0\
	&arsettings%5Bcollapsed%5D=1&arbuildyear%5Bmin%5D=\
	&arbuildyear%5Bmax%5D=&arpublished%5Bpublished%5D=1&offset='

EXAMPLE_SALES_REQUEST = 'http://asunnot.oikotie.fi/apartment-sell/search/card?\
	asprice%5Bsuffix%5D=000&asprice%5Bmin%5D=&asprice%5Bmax%5D=\
	&asprice%5Bunpriced%5D%5B%5D=1&assize%5Bmin%5D=\
	&assize%5Bmax%5D=&assettings%5Bchanged%5D=0\
	&assettings%5Bcollapsed%5D=1&asbuildyear%5Bmin%5D=\
	&asbuildyear%5Bmax%5D=&assizelot%5Bmin%5D=\
	&assizelot%5Bmax%5D=&asnewdevelopment%5Bnew_development%5D=1\
	&aspublished%5Bpublished%5D=1&offset=0&limit=48\
	&sortby=published%20desc&format=json'

EXAMPLE_RENTALS_REQUEST =  'http://asunnot.oikotie.fi/apartment-rent/search/card?\
	arpricerent%5Bmin%5D=&arpricerent%5Bmax%5D=&arsize%5Bmin%5D=\
	&arsize%5Bmax%5D=&arsettings%5Bchanged%5D=0\
	&arsettings%5Bcollapsed%5D=1&arbuildyear%5Bmin%5D=\
	&arbuildyear%5Bmax%5D=&arpublished%5Bpublished%5D=1&offset=\
	0&limit=24&sortby=published%20desc&format=json'


def build_req(offset, batch, mode):
	"""
	Return the request url. 
	offset: Results offset (i.e. index of first item to be fetched)
	batch: Number of results to fetch.
	mode: 0 for sales, 1 for rentals
	"""

	prefix = ""
	if mode == 0:
		prefix = SALES_REQUEST_PREFIX
	elif mode == 1:
		prefix = RENTALS_REQUEST_PREFIX
	else:
		raise Error("Invalid data mode {0} (0: sales, 1: rentals)".\
			format(mode))
	full_url = ''.join([prefix, str(offset), "&limit=",
		str(batch), "&sortby=published%20desc&format=json"])
	sys.stderr.write(''.join(['Full URL ', str(full_url), '\n']))
	return full_url

if __name__=='__main__':

	parser = parse.ArgumentParser()
	parser.add_argument('-b', type = int, default = 240,
		help = 'Batch size.')
	parser.add_argument('-w', type = int, default = 5,
		help = 'Number of SECONDS between requests.')
	parser.add_argument('-r', type = int, default = 0,
		help = 'Number of HOURS between redownload.')
	parser.add_argument('-m', type = int, choices = [0,1],
		help = 'Specify sales (0) or rentals (1).')
	parser.add_argument('-o', required = True,
		help = "Output filename with automatic .json extension.")
	args = parser.parse_args()

	mode_str = MODE_DICT[args.m]
	
	while(True): # keep parsing till the end of time
		
		# where to output results
		d0 = time.time() # now in seconds
		now = time.strftime("%d_%b_%Y_%H_%M_tz0000", time.gmtime())
		filename = "{0}_{1}_{2}.json".format(args.o, mode_str, now)
		output = open(filename, 'w')
	
		try:
			# initialization
			i = -1; offset = 0; batch = args.b
			while(True):
				i = i + 1
				# position of first result in batch
				offset = i * batch
				# build request url
				req = build_req(offset, batch, args.m)
				# submit request, get response
				# response = urllib2.urlopen(req)
				response = requests.get(req)
				# sys.stderr.write(''.join(['Request: ', str(response.geturl()),
				# 	'\n']))
				# read response - it's in json format
				# json_str = response.read()
				json_str = str(response.json()["response"])
				# write to output
				output.write(json_str + '\n')
				output.flush()
				sys.stderr.write('Finished writing this batch.\n')

				# have we downloaded all entries?
				# js = json.loads(str(json_str))
				# total_entries = js["response"]["numFound"]
				total_entries = response.json()["response"]["numFound"]

				if total_entries <= (i+1) * batch:
					break # time to stop
				
				# be gentle with the server
				sys.stderr.write('File: {0} [{1} / {2}] Going to nap.\n'\
					.format(filename, offset, total_entries))
				time.sleep(args.w)
		except Exception as e:
			sys.stderr.write(str(e))
			sys.stderr.write("\n")
			sys.stderr.write("File: {0} -- error at position ({1} - {2})\n".\
				format(filename, offset, offset+batch))

			exc_type, exc_obj, exc_tb = sys.exc_info()
			fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
			print(exc_type, fname, exc_tb.tb_lineno)
		
		output.flush(); output.close() # Done with this file

		if args.r > 0:
			# sleep for long time
			sys.stderr.write('[File: {0}] Big sleep -- {1} hours.\n'\
				.format(filename, args.r))
			d1 = time.time()
			time.sleep(3600 * args.r - (d1 - d0))
		else:
			break # will not parse again
	